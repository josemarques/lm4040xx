EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 5906 5906
encoding utf-8
Sheet 5 78
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L _NONAME_ U501
U 1 1 5EFB58E0
P 3100 2000
F 0 "U501" H 2900 1900 50  0000 C CNN
F 1 "" H 3100 2000 50  0001 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3100 1800 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm4040-n.pdf" H 3100 2000 50  0001 C CIN
	1    3100 2000
	-1   0    0    1   
$EndComp
$Comp
L _NONAME_ R501
U 1 1 5EFBBE12
P 2700 2000
F 0 "R501" H 2770 2046 50  0000 L CNN
F 1 "" H 2700 2000 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2630 2000 50  0001 C CNN
F 3 "~" H 2700 2000 50  0001 C CNN
	1    2700 2000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2450 2000 2550 2000
Wire Wire Line
	2850 2000 2900 2000
Wire Wire Line
	3250 2000 3400 2000
Wire Wire Line
	2900 1700 2900 2000
Connection ~ 2900 2000
Wire Wire Line
	2900 2000 2950 2000
$Comp
L _NONAME_ C502
U 1 1 5F0C5A35
P 3100 2450
F 0 "C502" V 2848 2450 50  0000 C CNN
F 1 "" H 3100 2450 50  0001 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3138 2300 50  0001 C CNN
F 3 "~" H 3100 2450 50  0001 C CNN
	1    3100 2450
	-1   0    0    1   
$EndComp
$Comp
L _NONAME_ C501
U 1 1 5F0C5A3B
P 2700 2450
F 0 "C501" V 2448 2450 50  0000 C CNN
F 1 "" H 2700 2450 50  0001 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2738 2300 50  0001 C CNN
F 3 "~" H 2700 2450 50  0001 C CNN
	1    2700 2450
	-1   0    0    1   
$EndComp
Wire Wire Line
	2700 2300 2900 2300
Wire Wire Line
	2700 2600 2900 2600
Wire Wire Line
	2900 2300 2900 2000
Connection ~ 2900 2300
Wire Wire Line
	2900 2300 3100 2300
Wire Wire Line
	2900 2700 2900 2600
Connection ~ 2900 2600
Wire Wire Line
	2900 2600 3100 2600
Wire Notes Line
	1900 1350 1900 3200
Wire Notes Line
	1900 3200 3800 3200
Wire Notes Line
	3800 3200 3800 1350
Wire Notes Line
	1900 1350 3800 1350
Text Notes 1950 3150 0    50   ~ 0
Reference voltage.\nActivate D6-> Meas A5-> \nanalogRead(A5)=1024*(V_REF/V_ALIM)
Text Label 2450 2000 2    50   ~ 0
V_CC
Text Label 2900 1700 1    50   ~ 0
V_RF
Text Label 3400 2000 0    50   ~ 0
GND
Text Label 2900 2700 3    50   ~ 0
GND
Text HLabel 4400 1550 0    50   Input ~ 0
V_RF
Text HLabel 4400 1650 0    50   Input ~ 0
V_CC
Text HLabel 4400 1750 0    50   Input ~ 0
GND
Text Label 4700 1550 0    50   ~ 0
V_RF
Text Label 4700 1650 0    50   ~ 0
V_CC
Text Label 4700 1750 0    50   ~ 0
GND
Wire Wire Line
	4400 1550 4700 1550
Wire Wire Line
	4400 1650 4700 1650
Wire Wire Line
	4400 1750 4700 1750
$EndSCHEMATC
